const posts = [
    {title: 'Post one', body:"This is first post"},
    {title: 'Post two', body:"This is second post"}
];

function getPosts(){
    //setTimeout takes in a callback fun and time to delay
    setTimeout(() => {
        let output = '';
        posts.forEach((post, index)=>{
            output += `<li>${post.title}</li>`;
            document.body.innerHTML = output;
        });
    }, 1000);   //Creating timeout of 1000ms
}

function createPost(post, callback){
    setTimeout(()=>{
        posts.push(post);
        callback(); //Called as soon as the post is created. Does wait for 2000ms timeout.
    }, 2000);
}

getPosts();

createPost({title:"Post three", body:"This is the third post"}, getPosts);